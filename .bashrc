# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ls='ls -lAF --color=always'
alias cls='clear'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
# Run twolfson/sexy-bash-prompt
. ~/.bash_prompt
